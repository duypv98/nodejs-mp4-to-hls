const { exec, spawn } = require('child_process');
const { promisify } = require('util');
const { writeFile, mkdir, stat } = require('fs');

module.exports = {
  execPromise: promisify(exec),

  spawnPromise: promisify(spawn),

  writeFilePromise: promisify(writeFile),

  mkdirPromise: promisify(mkdir)
}