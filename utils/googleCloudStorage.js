const { Storage, File } = require('@google-cloud/storage');
const path = require('path');
const { ReadStream } = require('fs');
const { GCS_BUCKET } = require('../config');

const keyFilename = `${path.resolve(__dirname, '..', 'secrets')}/ielts-fighters-eea0db66b2d9.json`;

const storage = new Storage({
  projectId: 'ielts-fighters',
  keyFilename,
});

const bucket = storage.bucket(GCS_BUCKET);

module.exports = {
  /**
   *
   * @param {String} filePath
   * @param {String} folderName
   * @param {String} fileName 
   */
  uploadFileInFolder: (filePath, folderName, fileName) => {
    return bucket.upload(filePath, {
      destination: `${folderName}/${fileName}`,
      gzip: true,
      metadata: {
        cacheControl: 'public, max-age=31536000'
      },
      public: true
    });
  },

  /**
   *
   * @param {String} bucketName 
   */
  createBucketIfNotExists: async (bucketName) => {
    const [existedBucket] = await bucket.exists();
    if (!existedBucket) {
      console.log('Bucket is not existed, creating...');
      const x = await bucket.create();
      console.log('Created bucket');
    } else {
      console.log('Bucket exists');
    }
  },

  /**
   * 
   * @param {String} dirPath 
   * @param {String} dirName 
   */
  gsutilUploadDirCommand: (dirPath, dirName) => `gsutil -m cp -a public-read -r ${dirPath} gs://${GCS_BUCKET}/${dirName}`,

  /**
   * 
   * @param {String} dirPath 
   * @param {String} dirName 
   * @param {String} filePattern 
   */
  gsutilUploadPatternCommand: (dirPath, dirName, filePattern) => `gsutil -m cp -a public-read -r ${dirPath}/${filePattern} gs://${GCS_BUCKET}/${dirName}`
}
