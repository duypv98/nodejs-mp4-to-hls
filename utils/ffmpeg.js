const ffmpeg = require('fluent-ffmpeg');
const ffmpegInstaller = require('@ffmpeg-installer/ffmpeg');
const fs = require('fs');
const path = require('path');

const videoServices = require('../services/video.services');
const { VideoStatus, SocketMessage } = require('../config');
const { gsutilUploadPatternCommand } = require('./googleCloudStorage');
const { execPromise } = require('./os');
const { sendMessage } = require('./socket');

ffmpeg.setFfmpegPath(ffmpegInstaller.path);

/**
 * 
 * @param {String} command 
 * @param {String} fileName 
 */
async function storeByCmd(command, fileName) {
  try {
    await execPromise(command);
    sendMessage(fileName, SocketMessage.SUCCESS);
    return videoServices.updateStatusStored(fileName);
  } catch (e) {
    console.error(e);
    sendMessage(fileName, SocketMessage.FAILED);
    return videoServices.updateStatus(fileName, VideoStatus.STORE_FAILED);
  }
}

/**
 * 
 * @param {String} fileStreamPath
 * @param {String} fileName 
 * @param {('480' | '720')} resolution 
 */
async function callback(fileStreamPath, fileName, resolution) {
  if (fs.existsSync(`${fileStreamPath}/${resolution}p.m3u8`)) {
    await videoServices.updateStatusFinished(fileName);

    const command = gsutilUploadPatternCommand(fileStreamPath, fileName, `${resolution}p*`);
    console.log(resolution);

    await storeByCmd(command, fileName);
  }
}

/**
 * 
 * @param {String} streamPath 
 * @param {('480' | '720')} resolution 
 */
function genOptions(streamPath, resolution) {
  let scale;
  let bv;
  let maxRate;
  let bufSize;

  if (resolution === '480') {
    scale = 'w=842:h=480';
    bv = '1400k';
    maxRate = '1498k';
    bufSize = '2100k';
  }

  if (resolution === '720') {
    scale = 'w=1280:h=720';
    bv = '2800k';
    maxRate = '2996k';
    bufSize = '4200k';
  }

  return [
    '-profile:v main',
    '-preset fast',
    `-vf scale=${scale}:force_original_aspect_ratio=decrease`,
    '-c:a aac',
    '-ar 48000',
    '-b:a 128k',
    // '-c:v h264_nvenc', // using GPU NVIDIA GTX 1050
    '-c:v h264',
    '-crf 20',
    '-g 48',
    '-keyint_min 48',
    '-sc_threshold 0',
    `-b:v ${bv}`,
    `-maxrate ${maxRate}`,
    `-bufsize ${bufSize}`,
    '-hls_time 30',
    `-hls_segment_filename ${streamPath}/${resolution}p_%03d.ts`,
    '-hls_playlist_type vod',
    '-f hls'
  ]
}

/**
 * 
 * @param {String} fileName 
 * @param {('480' | '720')} resolution 
 * @param {boolean} cb 
 */
module.exports = function encodeCmd(fileName, resolution, cb) {
  const fullFilePath = `${path.resolve(__dirname, '..', 'files')}/${fileName}`;
  const streamPath = path.resolve(__dirname, '..', 'streams');
  const fileStreamPath = `${streamPath}/${fileName}`;
  const options = genOptions(fileStreamPath, resolution);

  if (!cb) {
    return ffmpeg(fullFilePath)
      .addOptions(options)
      .output(`${fileStreamPath}/${resolution}p.m3u8`);
  }
  return ffmpeg(fullFilePath)
    .addOptions(options)
    .output(`${fileStreamPath}/${resolution}p.m3u8`)
    .on('end', () => callback(fileStreamPath, fileName, resolution));
}
