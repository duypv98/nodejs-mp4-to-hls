const SocketIO = require('socket.io');
const { Server } = require('http');

let io;

/**
 * 
 * @param {Server} srv 
 */
function initSocket(srv) {
  try {
    if (!io) {
      io = new SocketIO.Server(srv);
    }

    io.on('connection', socket => {
      console.log('Connected socket!');

      socket.on('room', room => {
        socket.join(room);
      });

      socket.on('leave-room', room => {
        socket.leave(room);
      });
    });

    console.log('Initiated socket!');
  } catch (e) {
    console.error(`Init socket failed: ${e}`)
  }
}

/**
 * 
 * @param {String} room 
 * @param {String} message 
 */
function sendMessage(room, message) {
  return io.sockets.in(room).emit('message', message);
}

module.exports = {
  initSocket,
  sendMessage
}


