const mongoose = require('mongoose');

const DB_NAME = 'videomng';
const DB_HOST = 
'localhost';
// '103.226.248.62';
const DB_USR = 'ks';
const DB_PWD = 'ks';

const DB_URL = `mongodb://${DB_USR}:${DB_PWD}@${DB_HOST}:27017/${DB_NAME}`

const connectDB = () => mongoose.connect(
  DB_URL,
  {
    authSource: DB_NAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  },
  (err) => {
    if (err) {
      console.error('Connect Failed', err);
    } else {
      console.info(`Connect Success at ${DB_URL}`);
    }
  }
)

module.exports = connectDB;
