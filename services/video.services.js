const { isValidObjectId } = require('mongoose');
const { VideoStatus } = require('../config');
const Video = require('../models/Video');

module.exports = {
  /**
   * 
   * @param {Number} status 
   */
  findByStatus: (status) => {
    if (!Object.values(VideoStatus).includes(status)) return null;
    return Video.find({ status });
  },

  /**
   * 
   * @param {Number} status 
   */
  findLatestByStatus: (status) => {
    if (!Object.values(VideoStatus).includes(status)) return null;
    return Video.findOne({ status }).sort('created_at');
  },

  /**
   * 
   * @param {String} idOrName 
   * @param {Number} newStatus 
   */
  updateStatus: (idOrName, newStatus) => {
    const filter = isValidObjectId(idOrName) ? { _id: idOrName } : { name: idOrName };
    return Video.findOneAndUpdate(filter, { status: newStatus })
  },

  /**
   * 
   * @param {String} idOrName 
   */
  updateStatusFinished: (idOrName) => {
    const filter = isValidObjectId(idOrName) ? { _id: idOrName } : { name: idOrName };
    return Video.findOneAndUpdate(filter, { status: VideoStatus.FINISHED, finished_at: Date.now() })
  },

  /**
   * 
   * @param {String} idOrName 
   */
  updateStatusStored: async (idOrName) => {
    const filter = isValidObjectId(idOrName) ? { _id: idOrName } : { name: idOrName };
    const video = await Video.findOne(filter);
    if (video && video.status !== VideoStatus.STORED) {
      return Video.findOneAndUpdate(filter, { status: VideoStatus.STORED, stored_at: Date.now() })
    }
  },

  /**
   * 
   * @param {String} name 
   * @param {String | Number} length 
   */
  create: (name, length) => Video.create({
    name,
    length,
    uploaded_at: null,
    finished_at: null,
    stored_at: null
  }),

  /**
   *
   * @param {String} name 
   */
  updateUploadedByName: (name) => Video.findOneAndUpdate({ name }, { status: VideoStatus.UPLOADED, uploaded_at: Date.now() }),

  /**
   *
   * @param {String} videoId 
   */
  findByIdOrName: (idOrName) => {
    const filter = isValidObjectId(idOrName) ? { _id: idOrName } : { name: idOrName };
    return Video.findOne(filter);
  },

  findOutdate: () => Video.find({
    $or: [
      { status: VideoStatus.STORED },
      { status: VideoStatus.STORED720}
    ]
  })
}
