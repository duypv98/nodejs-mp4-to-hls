const { CronJob } = require('cron');
const fs = require('fs');
const del = require('del');
const encodeCmd = require('./utils/ffmpeg');
const videoServices = require('./services/video.services');
const { VideoStatus, TIME_ZONE } = require('./config');

const encJob = new CronJob('* * * * *', async () => {
  const processingVideos = await videoServices.findByStatus(VideoStatus.PROCESSING);
  if (processingVideos.length === 0) {
    const lastRawVideo = await videoServices.findLatestByStatus(VideoStatus.UPLOADED);
    if (lastRawVideo) {
      const videoId = lastRawVideo._id;
      const videoName = lastRawVideo.name;
      await videoServices.updateStatus(videoId, VideoStatus.PROCESSING);

      encodeCmd(videoName, '480', true).run();
      encodeCmd(videoName, '720', true).run();
    }
  }
}, null, true, TIME_ZONE);

const delOutdateJob = new CronJob('0 0 * * *', async () => {
  const outdateVideos = await videoServices.findOutdate();
  console.log(outdateVideos);
  if (outdateVideos.length !== 0) {
    for (let i = 0; i < outdateVideos.length; i++) {
      let videoName = outdateVideos[i].name;
      if (fs.existsSync(`./files/${videoName}`)) {
        await del(`./files/${videoName}`);
      }

      if (fs.existsSync(`./streams/${videoName}`)) {
        await del(`./streams/${videoName}`);
      }

      await videoServices.updateStatus(outdateVideos[i]._id, VideoStatus.CLEANED);
    };
  }
}, null, true, TIME_ZONE);

module.exports = {
  encJob,
  delOutdateJob
}