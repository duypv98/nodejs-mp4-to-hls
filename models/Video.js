const mongoose = require('mongoose');
const { VideoStatus } = require('../config');

const videoSchema = new mongoose.Schema({
  name: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  length: Number,
  uploaded_at: Date,
  finished_at: Date,
  stored_at: Date,
  status: {
    type: Number,
    default: VideoStatus.NEW
  }
}, {
  timestamps: false,
  versionKey: false
});

const Video = mongoose.model('Video', videoSchema, 'videos');

module.exports = Video;
