module.exports = {
  VideoStatus: {
    NEW: 0,
    UPLOADED: 1,
    PROCESSING: 2,
    FINISHED: 3,
    STORED: 4,
    CLEANED: 5,
    FAILED: -1,
    STORE_FAILED: -2,
  },

  GCS_BUCKET: 'if-videos',
  TIME_ZONE: 'Asia/Ho_Chi_Minh',

  SocketMessage: {
    SUCCESS: 1,
    FAILED: -1
  }
}