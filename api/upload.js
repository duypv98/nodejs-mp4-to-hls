const express = require('express');
const checkVideoType = require('./middlewares/checkVideoType');
const uploader = require('./middlewares/tus');
// const checkPatchVideo = require('./middlewares/checkPatchVideo');
const uploadRoutes = require('./routers/upload.routes');
const videoServices = require('../services/video.services');

const router = express.Router();

router.use(
  uploadRoutes.UPLOAD,
  // checkVideoType,
  // checkPatchVideo,
  (req, res) => uploader.handle(req, res)
);

router.get(
  uploadRoutes.CHECK_STATUS,
  async (req, res, next) => {
    const videoId = req.query.videoId;
    if (!videoId) { res.status(400); return next(new Error('videoId is required')) }
    const video = await videoServices.findByIdOrName(videoId);

    if (!video) {
      res.status(404);
      return next(new Error('Video not found'));
    }

    return res.json({ status: video.status });
  }
)

module.exports = router;
