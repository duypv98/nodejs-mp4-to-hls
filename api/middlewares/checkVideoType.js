module.exports = (req, res, next) => {
  if (req.originalUrl === '/api/upload' && req.method === 'POST') {
    const contentType = req.headers['content-type'];
    if (!contentType || contentType !== 'video/mp4') return res.status(400).json({ success: false, msg: 'Unsupported media type'});
  }
  return next();
}