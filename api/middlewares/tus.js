const crypto = require('crypto');
const path = require('path');
const tus = require('tus-node-server');
const videoServices = require('../../services/video.services');
const googleCloudStorage = require('../../utils/googleCloudStorage');
const { mkdirPromise, writeFilePromise } = require('../../utils/os');

const {
  EVENT_FILE_CREATED,
  EVENT_UPLOAD_COMPLETE
} = tus.EVENTS;

const tusServer = new tus.Server();

tusServer.datastore = new tus.FileStore({
  path: '/files',
  namingFunction: () => {
    return `${Date.now()}-${crypto.randomBytes(16).toString('hex')}`;
  }
});

tusServer.on(EVENT_FILE_CREATED, ({ file: { id, upload_length } }) => videoServices.create(id, parseInt(upload_length)));

tusServer.on(EVENT_UPLOAD_COMPLETE, async ({ file: { id } }) => {
  const streamPath = path.resolve(__dirname, '..', '..', 'streams');
  await mkdirPromise(`${streamPath}/${id}`, { recursive: true });
  await writeFilePromise(
    `${streamPath}/${id}/index.m3u8`,
    `#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-STREAM-INF:BANDWIDTH=1400000,RESOLUTION=842x480\n480p.m3u8\n#EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720\n720p.m3u8`,
  );

  await googleCloudStorage.uploadFileInFolder(
    `${streamPath}/${id}/index.m3u8`,
    id,
    'index.m3u8'
  )
  await videoServices.updateUploadedByName(id);
});

module.exports = tusServer;
