const express = require('express');

const port = process.env.PORT || 3033
const { encJob, delOutdateJob } = require('./cron');
const connectDB = require('./utils/mongodb');
const { initSocket } = require('./utils/socket');

const PREFIX_API = '/api';
const APIs = [
  'upload'
];

const app = express();

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'X-PINGOTHER, Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
  next();
});

app.use(express.static('public', { index: 'index.html' }));

APIs.forEach(path => app.use(PREFIX_API, require(`./api/${path}`)));

app.use((req, res, next) => {
  return res.status(404).json({ success: false, msg: `Endpoint ${req.method} ${req.url} not found` });
});

app.use((err, req, res, next) => {
  const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
  res.status(statusCode);
  const error = { success: false, msg: err.message };

  console.log(err.stack);

  return res.json(error);
});

const server = app.listen(port, async () => {
  encJob.start();
  delOutdateJob.start();
  await connectDB();

  console.log(`Server is running on port ${port}`);
});

initSocket(server);
